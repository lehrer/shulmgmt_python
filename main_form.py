import datetime
import os
import re
import shutil
from shutil import copyfile

from flask import Flask, render_template, request
from flask_socketio import SocketIO, send, emit
from flask_wtf import FlaskForm
from wtforms import TextAreaField, SubmitField, SelectField
from wtforms.validators import DataRequired

from utilities import get_only_files_from_path, get_only_dirs_from_path, save_file_contents, get_file_contents, \
    read_config_file_return_configs_list

CONFIG_FILE='config.txt'
CONFIG_DICT=read_config_file_return_configs_list(os.path.dirname(os.path.realpath(__file__))+'\\'+CONFIG_FILE)
#BASEDIR= r'%s'%''.join(CONFIG_DICT['base_dir'])
BASEDIR= r'%s'%CONFIG_DICT['base_dir']
CONFIG_FILES_FOLDER = BASEDIR+'config_files\\'
BACKUP_CONFIG_FOLDER = BASEDIR + 'backup\\'

# App config.
DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
# Set the secret key to some random bytes. Keep this really secret!
# create new one $ python -c 'import os; print(os.urandom(16))'
app.secret_key = b'\x13\xa5\xda\x99\xb2\x12}\x1f:\r+\x96\xbe.l@'
app.config['CONFIG_FILES_FOLDER'] = CONFIG_FILES_FOLDER
app.config['BACKUP_CONFIG_FOLDER'] = BACKUP_CONFIG_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 100 * 1024 * 1024  # 100MB

# Set this variable to "threading", "eventlet" or "gevent" to test the
# different async modes, or leave it set to None for the application to choose
# the best option based on installed packages.
async_mode = None

socketio = SocketIO(app, async_mode=async_mode, ping_timeout=600)


class BackEndForm(FlaskForm):
    summernote = TextAreaField()
    #textFromSummernote = TextAreaField()
    save_button = SubmitField('Save', validators=[DataRequired()])

    choices = [('', '')]

    select_cell_to_edit = SelectField(
        'Select a cell to edit',
        choices=choices
        # test with this explicitly if required [('left_1', 'left_1'), ('middle_1', 'middle_1'), ('right_1', 'right_1')]
    )

    restore_options= [(0, 'Make a selection from the list')]


    backup_button= SubmitField('Backup current content')
    restore_select=SelectField('Select from the following backups',choices=restore_options)
    restore_button= SubmitField('Restore selected backup')
    reset_and_clear_button= SubmitField('Reset and clear all content')

@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response

@app.route('/admin', methods=['GET', 'POST'])
def admin():
    backend_form = BackEndForm()
    # if request.method == 'POST':
    #     print('post was issued')
    #     emit('fill_specific_cell',(request.form['select_cell_to_edit'],request.form['textFromSummernote']), namespace='/',broadcast=True)
    #     return redirect(request.url)
    cell_ids =CONFIG_DICT['cell_ids'].split(',')
    for id in cell_ids:
        print(id)
        backend_form.select_cell_to_edit.choices.append((id, id))

    onlydir = get_only_dirs_from_path(BACKUP_CONFIG_FOLDER)
    for f in onlydir:
        backend_form.restore_select.choices.append((f, f))
    return render_template('backend.html', backend_form=backend_form, async_mode=socketio.async_mode)


@app.route('/', methods=['GET', 'POST'])
def main():
    return render_template('frontend.html')


@socketio.on('message', namespace='/')
def handleMessage(msg):
    print('Message: ' + msg)
    send(msg, broadcast=True)


@socketio.on('fill specific cell', namespace='/')
def handleFillSpecificCell(cell, content):
    contentDetails = {
        'cell': cell,
        'content': content
    }
    emit('fill specific cell', contentDetails, namespace='/', broadcast=True)


@socketio.on('save content to file', namespace='/')
def handleSaveSummernoteToFile(cell, content):
    fullpath = app.config['CONFIG_FILES_FOLDER'] + cell + '.txt'
    save_file_contents(fullpath, content)


@socketio.on('select_event_raised', namespace='/')
def handleSelectEventRaised(cell):
    summernote_content = get_file_contents(app.config['CONFIG_FILES_FOLDER'] + cell + '.txt')
    emit('fill summernote panel', summernote_content, namespace='/', broadcast=True)


@socketio.on('fill all content', namespace='/')
def handleFillAllContent():
    print('fill all content')
    onlyfiles = get_only_files_from_path(CONFIG_FILES_FOLDER)
    for f in onlyfiles:
        print(f)
        handleFillSpecificCell(cell=f.replace('.txt', ''),content=get_file_contents(app.config['CONFIG_FILES_FOLDER'] + f))

@socketio.on('create backup', namespace='/')
def handleCreateBackup():
    timestamp=datetime.datetime.now().isoformat().replace(':', '_')
    backup_dir=r'{}{}\\'.format(app.config['BACKUP_CONFIG_FOLDER'],timestamp)
    print(backup_dir)
    os.makedirs(os.path.dirname(backup_dir))
    current_config__files=get_only_files_from_path(app.config['CONFIG_FILES_FOLDER'])
    for f in current_config__files:
        copyfile(app.config['CONFIG_FILES_FOLDER']+f,backup_dir+f)
    emit('backup added', os.path.basename(os.path.normpath(backup_dir)),namespace='/', broadcast=True)
    emit('send alert to backend.html', 'backup added ({})'.format(timestamp), namespace='/', broadcast=True)


@socketio.on('restore specific backup', namespace='/')
def handleRestoreSpecificBackup(backup_to_restore):
    backup_to_restore_dir=app.config['BACKUP_CONFIG_FOLDER']+backup_to_restore+'\\'
    backup_files = get_only_files_from_path(backup_to_restore_dir)
    current_config_files=app.config['CONFIG_FILES_FOLDER']
    if not os.path.exists(os.path.dirname(app.config['CONFIG_FILES_FOLDER'])):
        os.makedirs(os.path.dirname(app.config['CONFIG_FILES_FOLDER']))
    for f in backup_files:
        shutil.copy2(backup_to_restore_dir + f, current_config_files + f)
    emit('backup restored',namespace='/', broadcast=True)
    emit('send alert to backend.html','backup restored',namespace='/', broadcast=True)

@socketio.on('reset and clear', namespace='/')
def handleResetAndClear():
    if os.path.exists(os.path.dirname(app.config['CONFIG_FILES_FOLDER'])):
        shutil.rmtree(app.config['CONFIG_FILES_FOLDER'])
    emit('reset done', namespace='/', broadcast=True)
    emit('send alert to backend.html', 'all data was reset', namespace='/', broadcast=True)


if __name__ == "__main__":
    # app.run()
    socketio.run(app, port=5001)



