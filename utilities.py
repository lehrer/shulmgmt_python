import csv
import os

from os.path import isfile, join, isdir


def get_file_contents(fullpath):
    if os.path.exists(os.path.dirname(fullpath)):
        if os.path.isfile(fullpath): #does the file exist?
            f = open(fullpath, "r")
            return f.read()
    return ''

def get_rows_as_array_from_file(fullpath):
    values = []
    with open(fullpath, 'r', encoding='latin-1') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            value = ''.join(row)
            values.append(value.strip())
        return values

def read_config_file_return_configs_list(fullpath):
    config_dict = {}
    with open(fullpath) as f:
        for line in f.readlines():
            value = line.strip()
            try:
                if (value.__contains__('=')):
                    print('value: '+ value)
                    config_dict[value.split('=')[0]]=value.split('=')[1]
                    print(config_dict)
            except:
                pass
        return config_dict

def save_file_contents(fullpath, contents):
    if not os.path.exists(os.path.dirname(fullpath)):
        os.makedirs(os.path.dirname(fullpath))
    with open(fullpath, "w") as text_file:
        text_file.write(contents)

def get_only_files_from_path(full_path):
    if os.path.exists(os.path.dirname(full_path)):
        onlyfiles = [f for f in os.listdir(full_path) if isfile(join(full_path, f))]
        return onlyfiles
    return []



def get_only_dirs_from_path(full_path):
    if os.path.exists(os.path.dirname(full_path)):
        onlydirs = [f for f in os.listdir(full_path) if isdir(join(full_path, f))]
        return onlydirs
    return []


